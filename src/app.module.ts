import { InternalServerErrorException, MiddlewareConsumer, Module, ValidationPipe } from '@nestjs/common';

import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from './users/users.module';
import { ReportController } from './report/report.controller';
import { ReportModule } from './report/report.module';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { APP_PIPE } from '@nestjs/core';
import { ORMConfig } from './ormconfig';
const cookieSession = require('cookie-session')
@Module({
  imports: [
    ConfigModule.forRoot({ envFilePath: `.env.${process.env.NODE_ENV}`, isGlobal: true }),
    TypeOrmModule.forRoot(ORMConfig),
    UsersModule,
    ReportModule,
  ],
  controllers: [ReportController],
  providers: [
    {
      provide: APP_PIPE,
      useValue: new ValidationPipe({ whitelist: true, transform: true })
    },
  ],
})
export class AppModule {
  constructor(private configService: ConfigService) { }
  configure(consumer: MiddlewareConsumer) {
    const cookie_key = this.configService.get<string>('COOKIE_KEY')
    if (!cookie_key) throw new InternalServerErrorException('Cookie key doesnt exists')
    consumer.apply(cookieSession({
      keys: [cookie_key]
    })).forRoutes('*')

  }
}
