import { DataSource, DataSourceOptions } from 'typeorm';
import * as dotenv from 'dotenv';
import * as fs from 'fs';

const environment = process.env.NODE_ENV || 'development';
const data: any = dotenv.parse(fs.readFileSync(`.env.${environment}`));
const ORMConfig: any = {
    synchronize: false
}

switch (process.env.NODE_ENV) {
    case "development":
        Object.assign(ORMConfig,
            {
                type: 'postgres',
                host: data.POSTGRES_HOST,
                port: parseInt(data.POSTGRES_PORT),
                username: data.POSTGRES_USER,
                password: data.POSTGRES_PASSWORD,
                database: data.POSTGRES_DATABASE,
                entities: ["dist/**/*.entity{ .ts,.js}"],
                migrationsTableName: 'migration',
                migrations: ['migrations/*.js'],
                migrationsRun: true,
                cli: {
                    migrationsDir: 'migrations',
                },
            })
        break;
    case "production":
        Object.assign(ORMConfig,
            {
                type: 'postgres',
                url: process.env.DATABASE_URL,
                ssl: {
                    rejectUnauthorized: false
                },
                tls: {
                    rejectUnauthorized: false
                },
                entities: ["dist/**/*.entity{ .ts,.js}"],
                migrationsTableName: 'migration',
                migrations: ['migrations/*.js'],
                migrationsRun: true,
                cli: {
                    migrationsDir: 'migrations',
                },
            })
        break;
    case "test":
        Object.assign(ORMConfig, {
            type: "sqlite",
            database: 'test.sqlite',
            entities: ["dist/**/*.entity{ .ts,.js}"],
            synchronize: true
        })
        break;
    default:
        throw new Error('Unknown enviroment')
}

export { ORMConfig }

export default new DataSource(ORMConfig);