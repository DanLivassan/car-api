import { Injectable, InternalServerErrorException, NotFoundException } from '@nestjs/common';
import { Repository } from 'typeorm'
import { UserEntity } from './users.entity';
import { InjectRepository } from '@nestjs/typeorm'
import { createUserDto } from './dto/createUserDto';

@Injectable()
export class UsersService {
    constructor(@InjectRepository(UserEntity) private userRepository: Repository<UserEntity>) {
    }

    async create(data: createUserDto) {
        const user = this.userRepository.create(data)
        if (user)
            return await this.userRepository.save(user)
        throw new InternalServerErrorException(`Error: trying to save ${user}`)
    }

    async findAll() {
        return await this.userRepository.find()
    }

    async findOne(email: string) {
        const user = this.userRepository.findOneBy({ email })
        if (!user) throw new NotFoundException('NotFound: No user with the id: ' + email)
        return user
    }

    async findOneById(id: string) {
        if (!id) return null
        const user = this.userRepository.findOneBy({ id: parseInt(id) })
        if (!user) throw new NotFoundException('NotFound: No user with the id: ' + id)
        return user
    }


    async update(id: number, data: Partial<UserEntity>) {
        const user = await this.userRepository.findOneBy({ id })
        if (!user) throw new NotFoundException('NotFound: No user with the id: ' + id)
        Object.assign(user, data)
        return await this.userRepository.save(user)
    }

    async delete(id: number) {
        return await this.userRepository.delete({ id })
    }
}
