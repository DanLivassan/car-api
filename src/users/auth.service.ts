import { BadRequestException, Injectable, UnauthorizedException } from "@nestjs/common";
import { CryptoEncrypterAdapter } from "./encrypters/encrypter";
import { UsersService } from "./users.service";

@Injectable()
export class AuthService {
    constructor(private userService: UsersService, private encrypter: CryptoEncrypterAdapter) { }

    async signUp(email: string, password: string) {
        const user = await this.userService.findOne(email)
        if (user) throw new BadRequestException('This email already exists')
        const result = await this.encrypter.encryptOnSignUp(password)
        return await this.userService.create({ email, password: result })
    }
    async validate(email: string, password: string) {
        const user = await this.userService.findOne(email)
        if (!user) throw new UnauthorizedException('Invalid email and/or password')
        const [salt, hPassword] = user.password.split('.')
        const hash = await this.encrypter.encryptOnValidate(password, salt)
        if (hPassword == hash.toString('hex')) {
            return user
        }
        throw new UnauthorizedException('Invalid email and/or password')
    }
}