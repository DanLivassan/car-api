import {
    Body,
    Controller,
    Delete,
    Get,
    HttpCode,
    Param,
    Patch,
    Post,
    UseInterceptors,
    ClassSerializerInterceptor,
    Session,
    UseGuards,
} from '@nestjs/common';
import { SerializerInterceptor } from '../interceptors/serializer.interceptor';
import { AuthService } from './auth.service';
import { CurrentUser } from './decorators/current-user.decorator';
import { createUserDto, signinUserDto } from './dto/createUserDto';
import { updateUserDto } from './dto/updateUser.dto';
import { UserDto } from './dto/user.dto';
import { UserEntity } from './users.entity';
import { UsersService } from './users.service';
import { AuthGuard } from '../guards/auth.guard';

@Controller('users')
@UseInterceptors(new SerializerInterceptor(UserDto))
export class UsersController {
    constructor(private userService: UsersService, private authService: AuthService) { }
    @Post('')
    async create(@Body() body: createUserDto, @Session() session: any) {
        const user = await this.authService.signUp(body.email, body.password);
        session.userId = user.id
        return user
    }

    @Post('/signin')
    async signin(@Body() body: signinUserDto, @Session() session: any) {
        const user = await this.authService.validate(body.email, body.password);
        session.userId = user.id
        return user
    }
    @Post('/signout')
    async signout(@Session() session: any) {
        session.userId = null;
    }
    @Get('')
    @HttpCode(200)
    @UseInterceptors(ClassSerializerInterceptor)
    async list() {
        return await this.userService.findAll();
    }
    @UseGuards(AuthGuard)
    @Get('whoami')
    @HttpCode(200)
    @UseInterceptors(ClassSerializerInterceptor)
    async whoAmI(@CurrentUser() user: UserEntity) {
        return user
    }


    @HttpCode(200)
    @Get('/:id')
    @UseInterceptors(ClassSerializerInterceptor)
    async detail(@Param('id') id: string) {
        return await this.userService.findOneById(id);
    }
    @HttpCode(200)
    @Patch('/:id')
    async update(@Param('id') id: number, @Body() body: updateUserDto) {
        return await this.userService.update(id, body);
    }
    @HttpCode(204)
    @Delete('/:id')
    async remove(@Param('id') id: number) {
        return await this.userService.delete(id);
    }


}
