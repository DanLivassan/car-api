import { Expose } from "class-transformer";
import { ReportEntity } from "src/report/report.entity";

export class UserDto {
    @Expose()
    id: number;
    @Expose()
    email: string
    // @Expose()
    // reports: ReportEntity[]
}