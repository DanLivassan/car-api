import { IsEmail, IsOptional, IsStrongPassword } from 'class-validator'
export class updateUserDto {
    @IsOptional()
    @IsEmail()
    email: string;

    @IsOptional()
    @IsStrongPassword({ minLength: 5 })
    password: string;
}