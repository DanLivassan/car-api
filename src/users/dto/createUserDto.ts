import { IsEmail, IsString, IsStrongPassword } from 'class-validator'
export class createUserDto {
    @IsEmail()
    email: string;
    @IsStrongPassword({ minLength: 5 })
    password: string;
}

export class signinUserDto {
    @IsEmail()
    email: string;
    @IsString()
    password: string;
    @IsString()
    name: string;
}