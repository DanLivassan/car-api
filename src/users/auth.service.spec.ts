import { UnauthorizedException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { AuthService } from './auth.service';
import { CryptoEncrypterAdapter } from './encrypters/encrypter';
import { UserEntity } from './users.entity';
import { UsersService } from './users.service';

let MockUserService = {
    findOne: () => Promise.resolve({ id: 1, email: "email@mail.com", "password": "pass" }),
    create: (email: string, password: string) => Promise.resolve({ id: 1, email, password })
}

let MockCryptoService = {
    encryptOnValidate: jest.fn().mockResolvedValue('pass'),
    encryptOnSignUp: jest.fn().mockResolvedValue('pass')
}

describe('AuthService', () => {
    let sut: AuthService;

    beforeEach(async () => {
        MockUserService = {
            findOne: () => Promise.resolve({ id: 1, email: "email@mail.com", "password": "pass.pass" }),
            create: jest.fn().mockResolvedValue({ id: 1, 'email': 'email', password: 'oass' })
        }
        const module: TestingModule = await Test.createTestingModule({
            providers: [
                AuthService,
                { provide: UsersService, useValue: MockUserService },
                { provide: CryptoEncrypterAdapter, useValue: MockCryptoService }

            ]
        }).compile();

        sut = module.get<AuthService>(AuthService);
    });

    it('should be defined', () => {
        expect(sut).toBeDefined();
    });

    it('should hash the password on sign up', async () => {
        MockUserService.findOne = jest.fn().mockResolvedValue(undefined)
        MockCryptoService.encryptOnSignUp = jest.fn().mockResolvedValueOnce('otherpass')
        const user = await sut.signUp('email@mail.com', 'password')
        expect(sut).toBeDefined();
        expect(user).toBeDefined();
        expect(MockUserService.create).not.toBeCalledWith({ "email": "email@mail.com", "password": "pass" })
    });
    it('should throw if user doesnt exists on validate', async () => {
        MockUserService.findOne = jest.fn().mockResolvedValue(undefined)
        try {
            await sut.validate('email@mail.com', 'pass')
        }
        catch (e) {
            expect(e).toBeInstanceOf(UnauthorizedException)
        }
    });
    it('should return the user if password matches', async () => {
        let user: UserEntity | undefined;
        try {
            user = await sut.validate('email@mail.com', 'pass')
        }
        catch (e) {
            expect(e).not.toBeInstanceOf(UnauthorizedException)
        }
        finally {
            expect(user).toBeDefined()
        }
    });
});
