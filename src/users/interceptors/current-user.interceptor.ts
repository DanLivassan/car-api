import { Injectable, NestInterceptor, ExecutionContext, CallHandler, NestMiddleware } from '@nestjs/common';
import { Request } from 'express';
import { UserEntity } from '../users.entity';
import { UsersService } from '../users.service';

declare global {
    namespace Express {
        interface Request {
            currentUser: UserEntity
        }
    }
}
@Injectable()
export class CurrentUserInterceptor implements NestInterceptor {
    constructor(private userService: UsersService) { }
    async intercept(context: ExecutionContext, next: CallHandler): Promise<any> {
        const request = context.switchToHttp().getRequest()
        const { userId } = request.session || {}
        if (userId) {
            const user = await this.userService.findOneById(userId)
            request.currentUser = user
        }
        return next
            .handle()
    }
}

@Injectable()
export class CurrentUserMiddleware implements NestMiddleware {
    constructor(private userService: UsersService) {

    }
    async use(request: Request, res: any, next: (error?: any) => void) {
        const { userId } = request.session || {}

        if (userId) {
            const user = await this.userService.findOneById(userId)
            request.currentUser = user
        }
        return next()
    }

}