import { MiddlewareConsumer, Module } from '@nestjs/common';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthService } from './auth.service';
import { CryptoEncrypterAdapter } from './encrypters/encrypter';
import { CurrentUserMiddleware } from './interceptors/current-user.interceptor';
import { UsersController } from './users.controller';
import { UserEntity } from './users.entity';
import { UsersService } from './users.service';

@Module({
  imports: [TypeOrmModule.forFeature([UserEntity])],
  controllers: [UsersController],
  providers: [UsersService, CurrentUserMiddleware, AuthService, CryptoEncrypterAdapter],
  exports: [UsersService]
})
export class UsersModule {
  configure(consumer: MiddlewareConsumer) {

    consumer.apply(CurrentUserMiddleware).forRoutes('*')
  }
}
