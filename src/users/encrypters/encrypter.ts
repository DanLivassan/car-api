import { Injectable } from "@nestjs/common";
import { randomBytes, scrypt as _scrypt } from "crypto";
import { promisify } from "util";
const scrypt = promisify(_scrypt)

@Injectable()
export class CryptoEncrypterAdapter {
    async encryptOnSignUp(password: string) {
        const salt = randomBytes(8).toString('hex')
        const hash = await scrypt(password, salt, 32) as Buffer
        return salt + '.' + hash.toString('hex')
    }
    async encryptOnValidate(password: string, salt: string) {
        return await scrypt(password, salt, 32) as Buffer
    }
}