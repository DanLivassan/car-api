import { MockFactory, Test, TestingModule } from '@nestjs/testing';
import { UsersService } from './users.service';
import { Repository } from 'typeorm'
import { UserEntity } from './users.entity';
import { getRepositoryToken } from '@nestjs/typeorm';


// @ts-ignore
export const repositoryMockFactory: () => MockFactory<Repository<UserEntity>> = jest.fn(() => ({
  findOne: jest.fn(entity => entity),
  // ...
}));
describe('UsersService', () => {
  let service: UsersService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UsersService, { provide: getRepositoryToken(UserEntity), useFactory: repositoryMockFactory },
      ],
    }).compile();

    service = module.get<UsersService>(UsersService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
