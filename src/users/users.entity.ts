import { IsEmail, IsStrongPassword } from 'class-validator';
import { Column, Entity, JoinTable, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { ReportEntity } from 'src/report/report.entity';

export enum UserGroups {
    ADMIN = "ADMIN",
    REGULAR = 'REGULAR',
    UP = 'UP',
    TH = 'TH'
}
@Entity()
export class UserEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @IsEmail()
    @Column()
    email: string;

    @Column()
    name: string;

    @IsStrongPassword()
    @Column()
    password: string;

    @OneToMany(() => ReportEntity, (report) => report.user, { cascade: true })
    @JoinTable()
    reports: ReportEntity[]

    @Column({ type: 'enum', enum: UserGroups, default: UserGroups.REGULAR })
    groups: UserGroups
}
