import { CanActivate, ExecutionContext } from "@nestjs/common";
import { Observable } from "rxjs";
import { UserGroups } from "src/users/users.entity";

const ALLOWED = [UserGroups.ADMIN]
export class AdminGuard implements CanActivate {
    canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
        const request = context.switchToHttp().getRequest()
        const { group } = request.session.currentUser
        return ALLOWED.includes(group)
    }

}