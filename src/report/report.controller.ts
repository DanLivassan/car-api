import { Body, Controller, Delete, Get, HttpCode, Param, Patch, Post, Query, UseGuards } from '@nestjs/common';
import { AdminGuard } from '../guards/admin.guard';
import { AuthGuard } from '../guards/auth.guard';
import { CurrentUser } from '../users/decorators/current-user.decorator';
import { UserEntity } from '../users/users.entity';
import { CreateReportDto } from './dto/create-report.dto';
import { GetEstimateDto } from './dto/get-estimate.dto';
import { ReviewReportDto } from './dto/review-report.dto';
import { UpdateReportDto } from './dto/updateReport.dto';
import { ReportService } from './report.service';

@UseGuards(AuthGuard)
@Controller('report')
export class ReportController {
    constructor(private reportService: ReportService) {

    }
    @HttpCode(201)
    @Post('')
    async create(@Body() body: CreateReportDto, @CurrentUser() user: UserEntity) {
        return await this.reportService.create(body, user)
    }
    @HttpCode(200)
    @Get('')
    async list() {
        return await this.reportService.list()
    }

    @HttpCode(200)
    @Get('/estimate')
    async estimate(@Query() query: GetEstimateDto) {
        return await this.reportService.list(query)
    }

    @HttpCode(200)
    @Get('/:id')
    async detail(@Param('id') id: number) {
        return await this.reportService.findOne(id)
    }
    @HttpCode(200)
    @Patch('/:id')
    async update(@Param('id') id: number, @Body() body: UpdateReportDto) {
        return await this.reportService.update(id, body)
    }
    @UseGuards(AdminGuard)
    @HttpCode(200)
    @Patch('/:id/review')
    async review(@Param('id') id: number, @Body() body: ReviewReportDto) {
        return await this.reportService.update(id, body)
    }
    @HttpCode(204)
    @Delete('/:id')
    async remove(@Param('id') id: number) {
        return await this.reportService.delete(id)
    }
}
