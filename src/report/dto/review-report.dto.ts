import { IsEnum } from "class-validator";
import { ReportStatus } from "../report.entity";

export class ReviewReportDto {
    @IsEnum(ReportStatus)
    status: ReportStatus
}