import { Min, IsOptional } from "class-validator";

export class UpdateReportDto {
    id: number
    @IsOptional()
    @Min(0)
    price: number
}