import { IsLatitude, IsLongitude, IsString, Max, Min } from "class-validator";

export class CreateReportDto {
    @Min(0)
    price: number

    @IsString()
    make: string;

    @IsString()
    model: string;

    @Min(0)
    @Max(2050)
    year: number;

    @IsLatitude()
    lat: string;

    @IsLongitude()
    lng: string;

}