import { IsLatitude, IsLongitude, IsOptional, IsString, Max, Min } from "class-validator";
import { Transform } from "class-transformer";

export class GetEstimateDto {
    @IsOptional()
    @IsString()
    make: string;
    @IsOptional()
    @IsString()
    model: string;
    @Transform(({ value }) => parseInt(value))
    @IsOptional()
    @Min(0)
    @Max(2050)
    @Transform(({ value }) => parseInt(value))
    year: number;
    @IsOptional()
    @IsLatitude()
    lat: string;
    @IsOptional()
    @IsLongitude()
    lng: string;

}