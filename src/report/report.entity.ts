import { UserEntity } from "../users/users.entity";
import { Column, Entity, PrimaryGeneratedColumn, ManyToOne } from "typeorm";
import { IsEnum } from "class-validator";

export enum ReportStatus {
    APPROVED = 'APPROVED',
    PENDING = 'PENDING',
    REPROVED = 'REPROVED',
    PUBLISHED = 'PUBLISHED'
}
@Entity()
export class ReportEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    price: number;

    @Column()
    make: string;

    @Column()
    model: string;

    @Column()
    year: number;

    @Column()
    lat: string;

    @Column()
    lng: string;

    @ManyToOne(() => UserEntity, (user) => user.reports)
    user: UserEntity

    @IsEnum(ReportStatus)
    status: ReportStatus

    @IsEnum(ReportStatus)
    statuses: ReportStatus

}