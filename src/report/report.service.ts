import { Injectable, InternalServerErrorException, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm'
import { UserEntity } from 'src/users/users.entity';
import { Repository } from 'typeorm';
import { CreateReportDto } from './dto/create-report.dto';
import { ReportEntity } from './report.entity';
@Injectable()
export class ReportService {
    constructor(@InjectRepository(ReportEntity) private reportRepo: Repository<ReportEntity>) {
    }

    async create(data: CreateReportDto, user: UserEntity) {
        const report = this.reportRepo.create({ user: { email: user.email, id: user.id }, ...data })
        if (!report) throw new InternalServerErrorException('Error: Creating a report')
        return await this.reportRepo.save(report)
    }

    async list(query?: Partial<ReportEntity>) {
        if (!query)
            return await this.reportRepo.find()
        return await this.reportRepo.find({ where: query })
    }

    async findOne(id: number) {
        const report = this.reportRepo.findOneBy({ id })
        if (!report) throw new NotFoundException('NotFound: No report with the id: ' + id)
        return report
    }

    async update(id: number, data: Partial<ReportEntity>) {
        const report = await this.reportRepo.findOneBy({ id })
        if (!report) throw new NotFoundException('NotFound: No report with the id: ' + id)
        Object.assign(report, data)
        return await this.reportRepo.save(report)
    }

    async delete(id: number) {
        return await this.reportRepo.delete({ id })
    }
}
