const { MigrationInterface, QueryRunner } = require("typeorm");

module.exports = class initialSchema1677558899547 {
    name = 'initialSchema1677558899547'

    async up(queryRunner) {
        await queryRunner.query(`CREATE TYPE "public"."user_entity_groups_enum" AS ENUM('ADMIN', 'REGULAR', 'UP', 'TH')`);
        await queryRunner.query(`CREATE TABLE "user_entity" ("id" SERIAL NOT NULL, "email" character varying NOT NULL, "password" character varying NOT NULL, "groups" "public"."user_entity_groups_enum" NOT NULL DEFAULT 'REGULAR', CONSTRAINT "PK_b54f8ea623b17094db7667d8206" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "report_entity" ("id" SERIAL NOT NULL, "price" integer NOT NULL, "make" character varying NOT NULL, "model" character varying NOT NULL, "year" integer NOT NULL, "lat" character varying NOT NULL, "lng" character varying NOT NULL, "userId" integer, CONSTRAINT "PK_7862cb0d03ad2677692143cf38e" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "report_entity" ADD CONSTRAINT "FK_8f2828e57b5484726488f72ad58" FOREIGN KEY ("userId") REFERENCES "user_entity"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    async down(queryRunner) {
        await queryRunner.query(`ALTER TABLE "report_entity" DROP CONSTRAINT "FK_8f2828e57b5484726488f72ad58"`);
        await queryRunner.query(`DROP TABLE "report_entity"`);
        await queryRunner.query(`DROP TABLE "user_entity"`);
        await queryRunner.query(`DROP TYPE "public"."user_entity_groups_enum"`);
    }
}
