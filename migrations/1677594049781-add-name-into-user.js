const { MigrationInterface, QueryRunner } = require("typeorm");

module.exports = class addNameIntoUser1677594049781 {
    name = 'addNameIntoUser1677594049781'

    async up(queryRunner) {
        await queryRunner.query(`ALTER TABLE "user_entity" ADD "name" character varying NOT NULL`);
    }

    async down(queryRunner) {
        await queryRunner.query(`ALTER TABLE "user_entity" DROP COLUMN "name"`);
    }
}
