import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';
const mockUser = {
    email: "usernamefortest@mail.com",
    password: "@!MyGreatPassword123"
}
describe('AuthController (e2e)', () => {
    let app: INestApplication;

    beforeEach(async () => {
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [AppModule],
        }).compile();

        app = moduleFixture.createNestApplication();
        await app.init();
    });

    it('handle a signup request', () => {
        return request(app.getHttpServer())
            .post('/users/').send({ email: mockUser.email, password: mockUser.password })
            .expect(201)
            .then((res) => {
                const { id, email, password } = res.body
                expect(password).not.toBeDefined()
                expect(id).toBeDefined()
                expect(email).toBe('usernamefortest@mail.com')
            });
    });

    it('handle a signup with existent email', () => {
        return request(app.getHttpServer())
            .post('/users/').send({ email: mockUser.email, password: mockUser.password })
            .expect(201).then(() => {
                return request(app.getHttpServer())
                    .post('/users/').send({ email: mockUser.email, password: mockUser.password })
                    .expect(400)
            })

    });

    it('handle whoami ', async () => {
        const res = await request(app.getHttpServer())
            .post('/users/').send({ email: mockUser.email, password: mockUser.password })
            .expect(201)
        const cookie = res.get('Set-Cookie')
        console.log(cookie)
        return request(app.getHttpServer())
            .get('/users/whoami').set('Cookie', cookie)
            .expect(200).then(res => {
                const { email } = res.body
                expect(email).toBe(mockUser.email)
            })
    })



});
